variable "volumes" {
  description = "Volumes para mapear"
  type = list(object({
    container_path = string
    host_path      = string
    read_only      = bool
  }))
}

variable "ports" {
  description = "Portas para publicar"
  type = list(object({
    internal = number
    external = number
  }))
}

variable "image" {
  description = "Imagem de container para usar"
  type        = string
}

variable "name" {
  description = "Imagem de container para usar"
  type        = string
  default     = "container"
}

